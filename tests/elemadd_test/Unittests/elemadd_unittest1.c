#include <stdio.h>
#include <stdlib.h>
#include <string.h>  // For memset()
#include <assert.h>
#include "API.h"

// ... The provided elemadd_i8, dequantize, and quantize functions ...

void test_result(unsigned char *output, unsigned char *expected, unsigned long size) {
    for (unsigned long i = 0; i < size; i++) {
        assert(output[i] == expected[i]);
    }
    printf("Passed!\n");
}

int main() {
    unsigned long size = 5;  // sample size
    unsigned char input0[5] = {10, 20, 50, 200, 255};
    unsigned char input1[5] = {5, 10, 10, 100, 255};
    unsigned char output[5];

    float scale1[] = {1.0};
    float scale2[] = {2.0};
    unsigned char offset0[] = {0};
    unsigned char offset5[] = {5};
    unsigned char offset10[] = {10};


    printf("Test Case 1: Same scale and offset\n");
    memset(output, 0, size);
    elemadd_i8(input0, scale1, offset0, input1, scale1, offset0, output, scale1, offset0, size);
    unsigned char expected1[5] = {15, 30, 60, 255, 255};
    test_result(output, expected1, size);

    printf("Test Case 2: Different scale for input1\n");
    memset(output, 0, size);
    elemadd_i8(input0, scale1, offset0, input1, scale2, offset0, output, scale1, offset0, size);
    unsigned char expected2[5] = {20, 40, 70, 255, 255};
    test_result(output, expected2, size);

    printf("Test Case 3: Different offset for input1\n");
    memset(output, 0, size);
    elemadd_i8(input0, scale1, offset0, input1, scale1, offset5, output, scale1, offset0, size);
    unsigned char expected3[5] = {10, 25, 55, 255, 255};

    test_result(output, expected3, size);

    printf("Test Case 4: Different scale and offset for output\n");
    memset(output, 0, size);
    elemadd_i8(input0, scale1, offset0, input1, scale1, offset0, output, scale2, offset5, size);
    unsigned char expected4[5] = {12, 20, 35, 155, 255};

    test_result(output, expected4, size);

    printf("Test Case 5: Test underflow in quantize\n");
    unsigned char underflow_input0[5] = {5, 5, 5, 5, 5};
    unsigned char underflow_input1[5] = {1, 1, 1, 1, 1};
    memset(output, 0, size);
    elemadd_i8(underflow_input0, scale1, offset10, underflow_input1, scale1, offset10, output, scale1, offset0, size);
    unsigned char expected5[5] = {0, 0, 0, 0, 0};
    test_result(output, expected5, size);

    // ... Similarly for other test cases ...

    return 0;
}
