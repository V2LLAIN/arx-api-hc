#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "include/API.h"

typedef struct {
    float *image_data;
    unsigned char *label_data;
} MNISTData;

int mnist_datasets(MNISTData *dataset, const char *image_path, const char *label_path, int num_images);
float *load_32bit_float_array_from_file(const char *filename, size_t *size);
uint8_t *load_8bit_uint_array_from_file(const char *filename, size_t *size);
void save_32bit_int_to_file(int data, const char *filename);

int main(int argc, char** argv) {
    // 입력 이미지 선정
    MNISTData mnist_test[10000];

    if (mnist_datasets(mnist_test, "./data/images", "./data/labels", 10000) != 0) {
        fprintf(stderr, "Failed to load MNIST data\n");
        return 1;
    }

    // 파라미터 정의
    const char* output_names[] = {
        "y_scale0", "y_zero_point0",
        "q_weight1", "q_scale1", "q_zero_point1",
        "q_weight2", "q_scale2", "q_zero_point2",
        "y_scale1", "y_zero_point1",
        "q_weight3", "q_scale3", "q_zero_point3",
        "q_weight4", "q_scale4", "q_zero_point4",
        "y_scale2", "y_zero_point2",
        "q_weight5", "q_scale5", "q_zero_point5",
        "q_weight6", "q_scale6", "q_zero_point6",
        "y_scale3", "y_zero_point3"
    };

    float *data_0; uint8_t *data_1;
    uint8_t *data_2; float *data_3; uint8_t *data_4;
    uint8_t *data_5; float *data_6; uint8_t *data_7;
    float *data_8; uint8_t *data_9;
    uint8_t *data_10; float *data_11; uint8_t *data_12;
    uint8_t *data_13; float *data_14; uint8_t *data_15;
    float *data_16; uint8_t *data_17;
    uint8_t *data_18; float *data_19; uint8_t *data_20;
    uint8_t *data_21; float *data_22; uint8_t *data_23;
    float *data_24; uint8_t *data_25;

    size_t sizes[26];

    char filepaths[26][256];

    for (int i = 0; i < 26; ++i) {
        snprintf(filepaths[i], sizeof(filepaths[i]), "%s/%s.bin", "./param", output_names[i]);
    }

    data_0 = load_32bit_float_array_from_file(filepaths[0], &sizes[0]);
    data_1 = load_8bit_uint_array_from_file(filepaths[1], &sizes[1]);

    data_2 = load_8bit_uint_array_from_file(filepaths[2], &sizes[2]);
    data_3 = load_32bit_float_array_from_file(filepaths[3], &sizes[3]);
    data_4 = load_8bit_uint_array_from_file(filepaths[4], &sizes[4]);

    data_5 = load_8bit_uint_array_from_file(filepaths[5], &sizes[5]);
    data_6 = load_32bit_float_array_from_file(filepaths[6], &sizes[6]);
    data_7 = load_8bit_uint_array_from_file(filepaths[7], &sizes[7]);

    data_8 = load_32bit_float_array_from_file(filepaths[8], &sizes[8]);
    data_9 = load_8bit_uint_array_from_file(filepaths[9], &sizes[9]);

    data_10 = load_8bit_uint_array_from_file(filepaths[10], &sizes[10]);
    data_11 = load_32bit_float_array_from_file(filepaths[11], &sizes[11]);
    data_12 = load_8bit_uint_array_from_file(filepaths[12], &sizes[12]);

    data_13 = load_8bit_uint_array_from_file(filepaths[13], &sizes[13]);
    data_14 = load_32bit_float_array_from_file(filepaths[14], &sizes[14]);
    data_15 = load_8bit_uint_array_from_file(filepaths[15], &sizes[15]);

    data_16 = load_32bit_float_array_from_file(filepaths[16], &sizes[16]);
    data_17 = load_8bit_uint_array_from_file(filepaths[17], &sizes[17]);

    data_18 = load_8bit_uint_array_from_file(filepaths[18], &sizes[18]);
    data_19 = load_32bit_float_array_from_file(filepaths[19], &sizes[19]);
    data_20 = load_8bit_uint_array_from_file(filepaths[20], &sizes[20]);

    data_21 = load_8bit_uint_array_from_file(filepaths[21], &sizes[21]);
    data_22 = load_32bit_float_array_from_file(filepaths[22], &sizes[22]);
    data_23 = load_8bit_uint_array_from_file(filepaths[23], &sizes[24]);

    data_24 = load_32bit_float_array_from_file(filepaths[24], &sizes[24]);
    data_25 = load_8bit_uint_array_from_file(filepaths[25], &sizes[25]);

    uint8_t *data_2_nhwc = (uint8_t *)malloc(8*1*5*5 * sizeof(uint8_t));
    transpose_i8(data_2, data_2_nhwc, 8, 1, 5, 5,
                 8, 5, 5, 1,
                 0, 2, 3, 1);

    uint8_t *data_10_nhwc = (uint8_t *)malloc(16*8*5*5*sizeof(uint8_t));
    transpose_i8(data_10, data_10_nhwc, 16, 8, 5, 5,
                16, 5, 5, 8,
                0, 2, 3, 1);

    uint8_t *output1 = (uint8_t *)malloc(1*28*28*1 * sizeof(uint8_t));  // Quantize
    uint8_t *output2 = (uint8_t *)malloc(1*28*28*8 * sizeof(uint8_t));  // Conv + Add
    uint8_t *output3 = (uint8_t *)malloc(1*28*28*8 * sizeof(uint8_t));  // Relu
    uint8_t *output4 = (uint8_t *)malloc(1*14*14*8 * sizeof(uint8_t));  // MaxPool
    uint8_t *output5 = (uint8_t *)malloc(1*14*14*16 * sizeof(uint8_t)); // Conv + Add
    uint8_t *output6 = (uint8_t *)malloc(1*14*14*16 * sizeof(uint8_t)); // Relu
    uint8_t *output7 = (uint8_t *)malloc(1*4*4*16 * sizeof(uint8_t));   // MaxPool
    uint8_t *output8 = (uint8_t *)malloc(1*4*4*16 * sizeof(uint8_t));   // Transpose
    uint8_t *output9 = (uint8_t *)malloc(1*10 * sizeof(uint8_t));       // MatMul + Add
    float *output10 = (float *)malloc(1*10 * sizeof(float));            // Dequantize

    int count = 0;
    for (int i=0; i<100; ++i) {

        float *input_image = mnist_test[i].image_data;
        unsigned char *output_label = mnist_test[i].label_data;

        quantize(input_image, output1, 1*1*28*28, data_0, data_1);

        convolution_i8_scale(output1, data_0, data_1,
                            data_2_nhwc, data_3, data_4,
                            data_5, data_6, data_7,
                            output2, data_8, data_9,
                            1, 28, 28, 1,
                            8, 5, 5,
                            2, 1, 0, 1,
                            28, 28);

        relu(output2, output3, 1*28*28*8, data_9);

        maxpool_i8(output3, output4,
                    1, 28, 28, 8,
                    2, 2, 0, 2);

        convolution_i8_scale(output4, data_8, data_9,
                            data_10_nhwc, data_11, data_12,
                            data_13, data_14, data_15,
                            output5, data_16, data_17,
                            1, 14, 14, 8,
                            16, 5, 5,
                            2, 1, 0, 1,
                            14, 14);

        relu(output5, output6, 1*14*14*16, data_17);
        maxpool_i8(output6, output7,
                        1, 14, 14, 16,
                        3, 3, 0, 3);
        transpose_i8(output7, output8, 1, 4, 4, 16,
                1, 16, 4, 4,
                0, 3, 1, 2);

        fullyconnected_i8_scale(output8, data_16, data_17,
                data_18, data_19, data_20,
                data_21, data_22, data_23,
                output9, data_24, data_25,
                1, 256, 256, 10,
                1, 1, 10);
        dequantize(output9, output10, 10, data_24, data_25, 1, 0);

        int maxIndex = 0;
        float maxValue = output10[0];
        for (int i = 1; i < 10; ++i) {
            if (output10[i] > maxValue) {
                maxValue = output10[i];
                maxIndex = i;
            }
        }
        if(output_label[0] == maxIndex) {
            count = count + 1;
            // printf("전체:%d 정답:%d\n", i+1, count);
        }
    }
    count = (count / 10) * 10;
    save_32bit_int_to_file(count, "output.bin");

    return 0;
}

int mnist_datasets(MNISTData *dataset, const char *image_path, const char *label_path, int num_images)
{
    char image_file[256], label_file[256];
    size_t image_size, label_size;

    for (int i = 0; i < num_images; i++) {
        snprintf(image_file, sizeof(image_file), "%s/image%d.bin", image_path, i);
        snprintf(label_file, sizeof(label_file), "%s/label%d.bin", label_path, i);

        dataset[i].image_data = load_32bit_float_array_from_file(image_file, &image_size);
        if (dataset[i].image_data == NULL) {
            fprintf(stderr, "Failed to load image data for index %d\n", i);
            return 1;
        }

        dataset[i].label_data = load_8bit_uint_array_from_file(label_file, &label_size);
        if (dataset[i].label_data == NULL) {
            fprintf(stderr, "Failed to load label data for index %d\n", i);
            return 1;
        }
    }

    return 0;
}

float *load_32bit_float_array_from_file(const char *filename, size_t *size)
{
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(float);
    float *array = (float *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(float), *size, fp);
    fclose(fp);
    return array;
}

uint8_t *load_8bit_uint_array_from_file(const char *filename, size_t *size) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("Error opening file");
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *size = file_size / sizeof(uint8_t);
    uint8_t *array = (uint8_t *)malloc(file_size);
    if (array == NULL) {
        perror("Memory allocation failed");
        fclose(fp);
        return NULL;
    }

    fread(array, sizeof(uint8_t), *size, fp);
    fclose(fp);
    return array;
}

void save_32bit_int_to_file(int data, const char *filename) {
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        perror("Error opening file");
        return;
    }

    fwrite(&data, sizeof(data), 1, fp);
    fclose(fp);
}
