import os
import onnx
from onnx import numpy_helper
import numpy as np
import torch
import torch.nn.functional as F

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 부호 없는 정수 배열을 불러옴
def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

# 파일에 배열을 저장
def save_array_to_file(filename, data):
    data.tofile(filename)

# 양자화 파라미터 계산
def compute_quant_params(weight, axis=None):
    if axis is not None:
        # 축을 기준으로 최소/최대 값을 구합니다.
        min_val = weight.min(axis=axis, keepdims=True)
        max_val = weight.max(axis=axis, keepdims=True)
    else:
        # 전체 배열에 대한 최소/최대 값을 구합니다.
        min_val = weight.min()
        max_val = weight.max()

    # Scale과 zero_point를 계산합니다.
    scale = (max_val - min_val) / 255
    zero_point = np.round(-min_val / scale)

    # zero_point가 uint8 범위 내에 있는지 확인하고 조정합니다.
    zero_point = np.clip(zero_point, 0, 255)

    return scale.astype(np.float32), zero_point.astype(np.uint8)


def quantize(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), 0, 255).astype(np.uint8)

# 역양자화
def dequantize(input, scale, zero_point):
    return ((input.astype(np.int32) - np.int32(zero_point)) * scale).astype(np.float32)

# 합성곱
def QLinearConvAdd(x, x_scale, x_zero_point, w, w_scale, w_zero_point, bias, bias_scale, bias_zero_point):
    deq_x = dequantize(x, x_scale, x_zero_point)
    deq_w = dequantize(w, w_scale.reshape(-1,1,1,1), w_zero_point)
    deq_bias = dequantize(bias, bias_scale.reshape(1,-1,1,1), bias_zero_point)
    deq_y = F.conv2d(torch.from_numpy(deq_x), torch.from_numpy(deq_w), stride = 1, padding = 2)
    y_scale, y_zero_point = compute_quant_params(deq_y.numpy())
    deq_y = deq_y + torch.from_numpy(deq_bias)
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y, y_scale, y_zero_point

# 행렬곱
def QLinearMatMulAdd(a, a_scale, a_zero_point, b, b_scale, b_zero_point, bias, bias_scale, bias_zero_point):
    deq_a = dequantize(a, a_scale, a_zero_point)
    deq_b = dequantize(b, b_scale, b_zero_point)
    deq_bias = dequantize(bias, bias_scale, bias_zero_point)
    deq_y = torch.matmul(torch.from_numpy(deq_a), torch.from_numpy(deq_b))
    deq_y = deq_y + torch.from_numpy(deq_bias)
    y_scale, y_zero_point = compute_quant_params(deq_y.numpy())
    q_y = quantize(deq_y.numpy(), y_scale, y_zero_point)
    return q_y, y_scale, y_zero_point

def QLinearReLU(x, x_zero_point):
    x_move = x.astype(np.int32) - x_zero_point
    x_relu = F.relu(torch.from_numpy(x_move)).numpy()
    x_result = x_relu + x_zero_point
    return x_result

## 1.파라미터 추출 ##


# ONNX 모델 로드
model_path = './model/mnist-12.onnx'
model = onnx.load(model_path)

# param 폴더 생성
param_dir = './param'
if not os.path.exists(param_dir):
    os.makedirs(param_dir)

# 추출할 가중치 이름 리스트
weights_to_extract = ['Parameter5', 'Parameter6', 'Parameter87', 'Parameter88', 'Parameter193', 'Parameter194'] # float32 형태의 파라미터

# 추출할 가중치에 대해 순회하며 저장
for tensor in model.graph.initializer:
    if tensor.name in weights_to_extract:
        # NumPy 배열로 변환
        parameter_data = numpy_helper.to_array(tensor)
        # 바이너리 파일로 저장
        output_file_path = os.path.join(param_dir, tensor.name + '.bin')
        parameter_data.tofile(output_file_path)


## 2.가중치 양자화 파라미터 계산 ##


# bin 파일에서 데이터 불러오기
bin_path1 = os.path.join(param_dir, 'Parameter5.bin')
weight1 = load_32bit_float_array_from_file(bin_path1).reshape(8, 1, 5, 5)
bin_path2 = os.path.join(param_dir, 'Parameter6.bin')
weight2 = load_32bit_float_array_from_file(bin_path2).reshape(8, 1, 1)
bin_path3 = os.path.join(param_dir, 'Parameter87.bin')
weight3 = load_32bit_float_array_from_file(bin_path3).reshape(16, 8, 5, 5)
bin_path4 = os.path.join(param_dir, 'Parameter88.bin')
weight4 = load_32bit_float_array_from_file(bin_path4).reshape(16, 1, 1)
bin_path5 = os.path.join(param_dir, 'Parameter193.bin')
weight5 = load_32bit_float_array_from_file(bin_path5).reshape(16, 4, 4, 10).reshape(256, 10)
bin_path6 = os.path.join(param_dir, 'Parameter194.bin')
weight6 = load_32bit_float_array_from_file(bin_path6).reshape(1, 10)

# 가중치 양자화 및 파라미터 계산
q_scale1, q_zero_point1 = compute_quant_params(weight1,(1,2,3)) #.reshape(8,1,1,1)
q_weight1 = quantize(weight1, q_scale1, q_zero_point1)

q_scale2, q_zero_point2 = compute_quant_params(weight2,(0,1,2)) #.reshape(-1)
q_weight2 = quantize(weight2, q_scale2, q_zero_point2)

q_scale3, q_zero_point3 = compute_quant_params(weight3,(1,2,3)) #.reshape(16,1,1,1)
q_weight3 = quantize(weight3, q_scale3, q_zero_point3)

q_scale4, q_zero_point4 = compute_quant_params(weight4,(0,1,2)) #.reshape(-1)
q_weight4 = quantize(weight4, q_scale4, q_zero_point4)

q_scale5, q_zero_point5 = compute_quant_params(weight5,(0)) #.reshape(1,10)
q_weight5 = quantize(weight5, q_scale5, q_zero_point5)

q_scale6, q_zero_point6 = compute_quant_params(weight6,(0,1)) #.reshape(-1)
q_weight6 = quantize(weight6, q_scale6, q_zero_point6)

# 양자화된 가중치와 파라미터 저장
save_array_to_file(os.path.join(param_dir, 'q_weight1.bin'), q_weight1)
save_array_to_file(os.path.join(param_dir, 'q_weight2.bin'), q_weight2)
save_array_to_file(os.path.join(param_dir, 'q_weight3.bin'), q_weight3)
save_array_to_file(os.path.join(param_dir, 'q_weight4.bin'), q_weight4)
save_array_to_file(os.path.join(param_dir, 'q_weight5.bin'), q_weight5)
save_array_to_file(os.path.join(param_dir, 'q_weight6.bin'), q_weight6)

save_array_to_file(os.path.join(param_dir, 'q_scale1.bin'), q_scale1)
save_array_to_file(os.path.join(param_dir, 'q_scale2.bin'), q_scale2)
save_array_to_file(os.path.join(param_dir, 'q_scale3.bin'), q_scale3)
save_array_to_file(os.path.join(param_dir, 'q_scale4.bin'), q_scale4)
save_array_to_file(os.path.join(param_dir, 'q_scale5.bin'), q_scale5)
save_array_to_file(os.path.join(param_dir, 'q_scale6.bin'), q_scale6)

save_array_to_file(os.path.join(param_dir, 'q_zero_point1.bin'), q_zero_point1)
save_array_to_file(os.path.join(param_dir, 'q_zero_point2.bin'), q_zero_point2)
save_array_to_file(os.path.join(param_dir, 'q_zero_point3.bin'), q_zero_point3)
save_array_to_file(os.path.join(param_dir, 'q_zero_point4.bin'), q_zero_point4)
save_array_to_file(os.path.join(param_dir, 'q_zero_point5.bin'), q_zero_point5)
save_array_to_file(os.path.join(param_dir, 'q_zero_point6.bin'), q_zero_point6)


## 3.입출력 양자화 파라미터 계산 ##


# 데이터셋 경로 설정
image_path = './data/images'
label_path = './data/labels'
mnist_test = []

# 이미지와 라벨을 불러와 리스트에 추가
for i in range(10000):
    image_file = os.path.join(image_path, f'image{i}.bin')
    label_file = os.path.join(label_path, f'label{i}.bin')

    # 이미지 데이터 불러오기 및 형태 변환
    image_data = load_32bit_float_array_from_file(image_file).reshape(1, 1, 28, 28)

    # 라벨 데이터 불러오기
    label_data = load_8bit_int_array_from_file(label_file)

    # 이미지 데이터와 라벨 데이터를 튜플 형태로 리스트에 추가
    mnist_test.append((image_data, label_data))

count = 0
for i in range(1):
    # 테스트 데이터셋에서 하나의 이미지 선택
    input_image, output_label = mnist_test[6323] # mnist_test[6323]

    # 테스트 이미지 양자화 파라미터 계산
    y_scale0, y_zero_point0 = compute_quant_params(input_image, (0,1,2,3))

    # 실수 연산과 양자화 연산 추론 값 비교

    # 합성곱(1)
    output1 = F.conv2d(torch.from_numpy(input_image), torch.from_numpy(weight1), stride=1, padding=2)
    output2 = output1 + torch.from_numpy(weight2)
    # 합성곱(1-1)
    output11 = quantize(input_image, y_scale0, y_zero_point0)
    output21, y_scale1, y_zero_point1 = QLinearConvAdd(output11, y_scale0, y_zero_point0,
                                                        q_weight1, q_scale1, q_zero_point1,
                                                        q_weight2, q_scale2, q_zero_point2)

    # 활성화 함수(2)
    output3 = F.relu(output2)
    # 활성화 함수(2-1)
    # output31 = F.relu(torch.from_numpy(output21)).numpy()
    output31 = QLinearReLU(output21, y_zero_point1)

    # 최대풀링(3)
    output4 = F.max_pool2d(output3, kernel_size=2, stride=2, padding=0)
    # 최대풀링(3-1)
    output41 = F.max_pool2d(torch.from_numpy(output31), kernel_size=2, stride=2, padding=0).numpy()

    # 합성곱(4)
    output5 = F.conv2d(output4, torch.from_numpy(weight3), stride = 1, padding = 2)
    output6 = output5 + torch.from_numpy(weight4)
    # 합성곱(4-1)
    output51, y_scale2, y_zero_point2 = QLinearConvAdd(output41, y_scale1, y_zero_point1,
                                        q_weight3, q_scale3, q_zero_point3,
                                        q_weight4, q_scale4, q_zero_point4)

    # 활성화 함수(5)
    output7 = F.relu(output6)
    # 활성화 함수(5-1)
    # output61 = F.relu(torch.from_numpy(output51)).numpy()
    output61 = QLinearReLU(output51, y_zero_point2)

    # 최대풀링(6)
    output8 = F.max_pool2d(output7, kernel_size=3, stride=3, padding=0)
    # 최대풀링(6-1)
    output71 = F.max_pool2d(torch.from_numpy(output61), kernel_size=3, stride=3, padding=0).numpy()

    # 행렬곱(7)
    output9 = output8.reshape(1, 256)
    output10 = torch.matmul(output9, torch.from_numpy(weight5))
    output81 = output71.reshape(1, 256)
    # 행렬곱(7-1)
    output91, y_scale3, y_zero_point3 = QLinearMatMulAdd(output81, y_scale2, y_zero_point2,
                                        q_weight5, q_scale5, q_zero_point5,
                                        q_weight6, q_scale6, q_zero_point6)
    output101 = dequantize(output91, y_scale3, y_zero_point3)

    # 추론 검증
    if (output_label == np.argmax(output101[0])):
        count = count + 1
        # print('전체:', i+1, ' 정답:', count)

# 입출력 양자화 파라미터 저장
save_array_to_file(os.path.join(param_dir, 'y_scale0.bin'), y_scale0)
save_array_to_file(os.path.join(param_dir, 'y_scale1.bin'), y_scale1)
save_array_to_file(os.path.join(param_dir, 'y_scale2.bin'), y_scale2)
save_array_to_file(os.path.join(param_dir, 'y_scale3.bin'), y_scale3)

save_array_to_file(os.path.join(param_dir, 'y_zero_point0.bin'), y_zero_point0)
save_array_to_file(os.path.join(param_dir, 'y_zero_point1.bin'), y_zero_point1)
save_array_to_file(os.path.join(param_dir, 'y_zero_point2.bin'), y_zero_point2)
save_array_to_file(os.path.join(param_dir, 'y_zero_point3.bin'), y_zero_point3)
