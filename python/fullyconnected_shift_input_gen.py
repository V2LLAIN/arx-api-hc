import numpy as np
# import sys

import torch
import torch.nn.functional as F

MAX_INPUT_ELEMENT = 15
MAX_INPUT_DIM_LENGTH = 4
MAX_WEIGHT_ELEMENT = 3
MAX_OUTPUT_DIM_LENGTH = 4
MAX_BIAS_ELEMENT = 3
MAX_SHIFT = 3
DO_BIAS = True

class Network(torch.nn.Module):
    def __init__(self, input_size, output_size, weight, bias):
        super(Network, self).__init__()
        self.fc = torch.nn.Linear(input_size, output_size)
        self.fc.weight = torch.nn.Parameter(weight)
        self.fc.bias = torch.nn.Parameter(bias)
    def forward(self, x):
        x = self.fc(x)
        return x

def create_random_8bit_input_tensor(max_dimension_length, max_element_size):
    # Define input tensor (batch_size, channels, height, width) with random numbers
    in_dim0 = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    in_dim1 = np.random.randint(3, max_dimension_length, dtype=np.uint8)
    return in_dim0, in_dim1, torch.randint(1, max_element_size, (in_dim0, in_dim1), dtype=torch.uint8)

def create_random_8bit_weight_tensor(max_element_size, in_dim0, in_dim1, out_dim0, out_dim1):
    # Define input tensor (batch_size, channels, height, width) with random numbers
    kernel_dim0 = out_dim0 * out_dim1
    kernel_dim1 = in_dim0 * in_dim1
    return kernel_dim0, kernel_dim1, torch.randint(0, max_element_size + 1, (kernel_dim0, kernel_dim1), dtype=torch.uint8)

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def load_8bit_int_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)


def main():

    # define input tensor
    in_dim0, in_dim1, input_tensor = create_random_8bit_input_tensor(MAX_INPUT_DIM_LENGTH, MAX_INPUT_ELEMENT)

    # define argument tensor
    out_dim0 = np.random.randint(3, MAX_OUTPUT_DIM_LENGTH, dtype=np.uint8)
    out_dim1 = np.random.randint(3, MAX_OUTPUT_DIM_LENGTH, dtype=np.uint8)
    
    # define weight tensor
    kernel_dim0, kernel_dim1, kernel_tensor = create_random_8bit_weight_tensor(MAX_WEIGHT_ELEMENT, in_dim0, in_dim1, out_dim0, out_dim1)

    # define bias tensor
    bias_tensor = torch.randint(0, MAX_BIAS_ELEMENT + 1, size=(), dtype=torch.uint8)


    # define arguments array
    shift = np.random.randint(0, MAX_SHIFT + 1, dtype=np.uint8)
    arguments = np.array([in_dim0, in_dim1, kernel_dim0, kernel_dim1, DO_BIAS, shift, out_dim0, out_dim1], dtype=np.uint8)

    # tensor를 flatten한 후 binary file로 저장
    save_8bit_int_array_to_file("input.bin", input_tensor.reshape(-1).numpy())
    save_8bit_int_array_to_file("kernel.bin", kernel_tensor.reshape(-1).numpy())
    save_8bit_int_array_to_file("bias.bin", bias_tensor.numpy())
    save_8bit_int_array_to_file("arguments.bin", arguments)

    # 다시 input을 불러오고 2D로 reconstruct
    input_tensor = torch.tensor(load_8bit_int_array_from_file("input.bin")).view(in_dim0, in_dim1)
    kernel_tensor = torch.tensor(load_8bit_int_array_from_file("kernel.bin")).view(kernel_dim0, kernel_dim1)
    bias_tensor = torch.tensor(load_8bit_int_array_from_file("bias.bin"))

    # fully connected operation
    output_tensor = input_tensor.to(torch.int32).reshape(-1) @ kernel_tensor.to(torch.int32)
    output_tensor = torch.add(output_tensor, bias_tensor)

    # shift operation
    output_tensor = output_tensor >> shift
    output_tensor = output_tensor.to(torch.uint8)


    save_8bit_int_array_to_file("golden.bin", output_tensor.reshape(-1).numpy())

if __name__ == "__main__":
    main()