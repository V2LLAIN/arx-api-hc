import numpy as np
import sys

def create_random_32bit_float_array(length):
    # 주어진 길이만큼의 32-bit 실수 배열을 random한 값으로 생성
    return np.random.uniform(low=-5.0, high=5.0, size=length).astype(np.float32)

def load_32bit_float_array_from_file(filename):
    # 파일에서 32-bit 실수 배열을 불러옴
    return np.fromfile(filename, dtype=np.float32)

def load_8bit_uint_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

def save_array_to_file(filename, data):
    # 파일에 배열을 저장
    data.tofile(filename)

def calculate_scale_and_offset(input_data, qmin, qmax):
    min_val = min(input_data)
    max_val = max(input_data)
    scale = (max_val - min_val) / (qmax - qmin)
    offset = qmin - round(min_val / scale)
    return np.array([scale, offset], dtype = np.float32)

def quantize_arrays(input_data, scale, offset):
    size = len(input_data)
    output = np.array([0] * size, dtype = np.uint8)
    for i in range(size):
        quantized = input_data[i] / scale + offset
        output[i] = min(max(int(quantized), 0), 255)
    return output.astype(np.uint8)

def dequantize_arrays(quantize_data, scale, offset):
    size = len(quantize_data)
    output = np.array([0.0] * size, dtype=np.float32)
    for i in range(size):
        output[i] = (quantize_data[i] - offset) * scale
    return output.astype(np.float32)

def main():
    if len(sys.argv) != 2:
        length = 10
    else:
        length = int(sys.argv[1])

    # 주어진 길이만큼의 random한 32-bit integer array 생성
    array1 = create_random_32bit_float_array(length)
    save_array_to_file("array1.bin", array1)
    param1 = calculate_scale_and_offset(array1, 0, 255)

    # 배열 양자화 연산
    input1 = quantize_arrays(array1, param1[0], param1[1])

    # 배열을 파일에 저장
    save_array_to_file("input1.bin", input1)
    save_array_to_file("param1.bin", param1)

    # 저장된 배열을 다시 불러옴
    input1 = load_8bit_uint_array_from_file("input1.bin")
    param1 = load_32bit_float_array_from_file("param1.bin")

    # 배열 역양자화 연산
    result = dequantize_arrays(input1, param1[0], param1[1])

    # 결과를 출력 파일에 저장
    save_array_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
