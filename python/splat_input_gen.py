import numpy as np
import sys

def load_8bit_int_value_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

def save_8bit_int_to_file(filename, data):
    # 파일에 8-bit 정수 값을 저장
    data.tofile(filename)

def splat_arrays(size, value):
    # splat 연산
    result = np.full(size, value, dtype=np.uint8)
    return result

def main():
    if len(sys.argv) != 3:
        size = 100
        value = 100
    else:
        size = int(sys.argv[1])
        value = int(sys.argv[2])

    # 단일 값으로 이루어진 배열 생성
    array1 = np.array([value], dtype=np.uint8)

    # 배열을 input1.bin을 파일에 저장
    save_8bit_int_to_file("input1.bin", array1)

    # 저장된 배열을 다시 불러옴
    array1 = load_8bit_int_value_from_file("input1.bin")

    # 불러온 첫 번째 값을 사용
    value = loaded_value = array1[0] if array1.size > 0 else 0

    # splat 연산
    result = splat_arrays(size, value)

    # 결과를 출력 파일에 저장
    save_8bit_int_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
