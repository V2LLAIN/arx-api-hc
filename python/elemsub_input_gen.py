import numpy as np
import sys

def create_random_8bit_int_array(length):
    # 주어진 길이만큼의 8-bit 정수 배열을 random한 값으로 생성
    return np.random.randint(0, 255, size=length, dtype=np.uint8)

def load_8bit_int_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

def save_8bit_int_array_to_file(filename, data):
    # 파일에 8-bit 정수 배열을 저장
    data.tofile(filename)

def sub_arrays(array1, array2):
    # 배열 뺄셈. 오버플로우를 처리하기 위해 int16로 타입 변환 후, 다시 int8로 복귀
    result = np.clip((array1.astype(np.int16) - array2.astype(np.int16)), 0, 255).astype(np.uint8)
    return result

def main():
    if len(sys.argv) != 2:
        length = 100
    else:
        length = int(sys.argv[1])

    # 주어진 길이만큼의 random한 8-bit integer array 생성
    array1 = create_random_8bit_int_array(length)
    array2 = create_random_8bit_int_array(length)

    # 배열을 input1.bin과 input2.bin 파일에 저장
    save_8bit_int_array_to_file("input1.bin", array1)
    save_8bit_int_array_to_file("input2.bin", array2)

    # 저장된 배열을 다시 불러옴
    array1 = load_8bit_int_array_from_file("input1.bin")
    array2 = load_8bit_int_array_from_file("input2.bin")

    # 두 배열의 길이가 동일한지 확인
    assert len(array1) == len(array2), "The two arrays have different lengths"

    # 배열 뺄셈
    result = sub_arrays(array1, array2)

    # 결과를 출력 파일에 저장
    save_8bit_int_array_to_file("golden.bin", result)

if __name__ == "__main__":
    main()
