#include <stdio.h>
#include "include/ONES_MATH.h"


float ones_exp(float x, int terms) {

    float result = 1.0f;
    float term = 1.0f;

    if (x == 0.0f) {
        return 1.0f; // 입력이 0인 경우 예외 처리
    }
    if (x < 0.0f) {
        // 입력이 음수인 경우 양수로 변환
        x = -x;
    }
    for (int i = 1; i < terms; i++) {
        term *= x / (float)i;
        result += term;
    }
    return 1.0f / result;
}


float ones_tanh(float x, int terms) {
    float exp_pos = ones_exp(x, terms);
    float exp_neg = ones_exp(-x, terms);
    return (exp_pos - exp_neg) / (exp_pos + exp_neg);
}


float ones_round(float number) {
    int intPart = (int)number; 
    float fracPart = number - intPart; 

    if (fracPart == 0.5 || fracPart == -0.5) {
        if (intPart % 2 == 0) {
            return intPart;
        }
        return (number > 0) ? intPart + 1 : intPart - 1;
    }
    
    if (fracPart >= 0.5 || fracPart <= -0.5) {
        return (number > 0) ? intPart + 1 : intPart - 1;
    } else {
        return intPart;
    }
}

float ones_floorf(float x) {
    if (x >= 0) {
        return (int)x;  // 양의 실수의 경우, 정수 부분만 반환
    } else {
        int ix = (int)x;  // 음의 실수의 경우, 더 작은 정수로 내림
        return (float)(x == ix ? ix : ix - 1);
    }
}

